# -*- coding: utf-8 -*-
"""
Created on Wed May 30 12:40:08 2018

@author: kanagarw
"""

#Imports
import pandas as pd
import os
#import statsmodels.api as sm
from flask import Flask
import requests
from datetime import datetime
import dill
import json

folder=os.getcwd()

app = Flask(__name__)

@app.route('/')
def ds():
    #Transformed Files
    #tx_file=pd.read_csv(os.path.join('/storage','Transformed_2018-06-04_17-30-33.csv'),parse_dates=['Date'], index_col=['Date'])
    deRead = "localhost:8080/read?timestamp=2018-06-06_03-28-47"
    try:
        uResponse = requests.get(deRead)
    except requests.ConnectionError:
       print ("Connection Error")
    Jresponse = uResponse.text
    data = json.loads(Jresponse)
    tx_file=pd.DataFrame(data)
    #Prophet predictions
    #fit1 = sm.tsa.statespace.SARIMAX(tx_file['United Kingdom'], order=(0, 1, 1),enforce_stationarity=False).fit()
    #forecast=fit1.predict(dynamic=True)
        
    #Dump model
    def m(x): return x.mean()
    filename = os.path.join('/storage'+'/finalized_model.sav')
    ouf = open(filename, 'wb')
    dill.dump(m, ouf)
    ouf.close()    
    
    forecast=m(tx_file['United Kingdom'])
    
    #Dump Predictions
    pd.DataFrame(forecast).to_csv('/storage'+'forecast.csv')
      
    return "Forecasts saved!"


app.run(host="0.0.0.0", port=int("8081"), debug=True)


