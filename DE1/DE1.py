# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#Imports
import pandas as pd
from flask import Flask,request
import os
from datetime import datetime
folder=os.getcwd()

app = Flask(__name__)

@app.route('/transform')
def de1():
    #Raw Files picked from Database, currently under Iview
    raw_file=pd.read_csv(os.path.join(folder, 'raw_files/_visitors.csv'))
    #print (raw_file.head())
    
    #Transformations
    
    tx_file=raw_file[['Date','United Kingdom']]
    #Freeing Space
    del(raw_file)
    
    tx_file['Date']=tx_file['Date'].astype(str).map(lambda x:x.split('Q')[0])
    tx_file=tx_file.groupby('Date').sum()
    mydir = os.path.join('/storage', 'Transformed_'+datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
    os.makedirs(mydir)
    #File dumped
    tx_file.to_csv(os.path.join(mydir,'transformed.csv'))
    print ("wow")
    return "Transformation done!"

@app.route('/read')
def deread():
    timestmp = request.args.get('timestamp')
    tx_file=pd.read_csv(os.path.join('/storage','Transformed_'+timestmp+'/','transformed.csv'),parse_dates=['Date'], index_col=['Date'])
    return tx_file.to_json()

app.run(host="0.0.0.0", port=8080, debug=True)


